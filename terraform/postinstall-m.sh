#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

exec > /tmp/debug.$$ 2>&1
set -x

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
#apt update && apt -y install git jq nginx
apt update && apt -y install git jq nginx ceph-common lvm2

# hostname
hostnamectl set-hostname master --static

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux

# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOFAcEobnJc+o0jEBN1t/rBKU+scMHPtG4Keail7rJhs micuper
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

## nginx
apt -y install nginx
sed -i 's/listen 80/listen 8888/g' /etc/nginx/sites-available/default
systemctl restart nginx 
#

## Init K3S
# Variables
export INSTALL_K3S_VERSION="v1.29.0+k3s1"
# Install
#curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--disable=traefik" sh -
curl -sfL https://get.k3s.io | sh -s server - --cluster-init --disable traefik --etcd-snapshot-schedule-cron "*/30 * * * *" --etcd-snapshot-retention "10"


# kubeconfig
mkdir -p /root/.kube
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml
cp -i /etc/rancher/k3s/k3s.yaml /root/.kube/config
chmod +r /etc/rancher/k3s/k3s.yaml
echo "export KUBECONFIG=/etc/rancher/k3s/k3s.yaml" >> /etc/bashrc
cp /var/lib/rancher/k3s/server/token /var/www/html/token
chmod 644 /var/www/html/token
#

## Ingress Controller deployment
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.8.2/deploy/static/provider/baremetal/deploy.yaml
sed -i "s/NodePort/LoadBalancer/g" deploy.yaml
sed -i "/LoadBalancer/a\  externalIPs:\n    - $LBIP" deploy.yaml
kubectl apply -f deploy.yaml
##

## Install Helm
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
##

## Install kubens
wget https://github.com/ahmetb/kubectx/releases/download/v0.9.4/kubens_v0.9.4_linux_x86_64.tar.gz
tar xvzf kubens_v0.9.4_linux_x86_64.tar.gz
mv kubens /usr/local/bin/
##

## Metrics Server
#kubectl apply -f https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
##

:'
## Rook/ceph
rook_ver=v1.10.5

# rook operator
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/crds.yaml
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/common.yaml
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/operator.yaml
sleep 3

# form clusterrook-ceph.example.com
wget https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/cluster.yaml
sed -i "/network:/a\    hostNetwork: true" cluster.yaml
sed -i "/# encryptedDevice: /a\      encryptedDevice: \"true\"" cluster.yaml
#kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/cluster.yaml
kubectl apply -f cluster.yaml
sleep 5

# install toolbox
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/toolbox.yaml

# expose dashboard
kubectl apply -f https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/dashboard-external-https.yaml
wget https://raw.githubusercontent.com/rook/rook/${rook_ver}/deploy/examples/dashboard-ingress-https.yaml
sed -i 's/rook-ceph.example.com/rook.opensovereigncloud.com/g' dashboard-ingress-https.yaml  
kubectl apply -f dashboard-ingress-https.yaml
##
'



## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
EOF
source /etc/bashrc

cat <<EOF | tee -a /home/tux/.bashrc
# Aliases
alias s='sudo su -'
alias k='k3s kubectl'
alias c=clear
export PATH=$PATH:/usr/local/go/bin
EOF
chown tux:tux /home/tux/.bashrc 
source /home/tux/.bashrc
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││                 Welcome to OSC Environment!               ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││                      K3s Cluster                          ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
###
